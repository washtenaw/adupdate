
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.sql.*;
import java.util.*;

public class ADUpdate {

	public static void main(String[] args) {
		//System.out.println("starting");
		 String FileName = "adupdate.log";
	    	       
		ADContext context = new ADContext();
		Vector<MunisPerson> munisPeople = new Vector<MunisPerson>();
//		PrintWriter out = null;
		PrintStream out = null;
		
		
		try {
			FileOutputStream fos = new FileOutputStream(new File(FileName));
			out = new PrintStream(fos);
			
			String connectionUrl = "jdbc:sqlserver://munissql:1435;"
					+ "database=munprod;"
					+"user=ob_munis_user;"
					+ "password=U-P0HV7e9eUpj87z1TZx-3nGafMkDexCkBvMrb1M;"
					+ "encrypt=true;"
					+ "trustServerCertificate=true;";
			
			try (Connection conn = DriverManager.getConnection(connectionUrl);){
				Statement stmt = conn.createStatement();

				ResultSet rs = null; 

				String sql = "select * from wc.wc_emp_topdesk order by employeenumber";
		//		String sql = "select * from wc.wc_emp_topdesk where employeenumber = 377455";

				rs = stmt.executeQuery(sql);

				while (rs.next()) {
					//out.println(rs.getString("username"));

					munisPeople.add(new MunisPerson(
							rs.getString("Primaryemail"), 
							rs.getString("EmployeeNumber"), 
							rs.getString("department"),
							rs.getString("supervisoremail"),
							rs.getString("departmentHead")));
				} // end while

				
				MunisPerson tPerson = null;
		
				for (int x=0;x < munisPeople.size(); x++) {
		
					tPerson = munisPeople.elementAt(x);
		
					int loc = tPerson.getEmail().indexOf("@");
					String username = tPerson.getEmail().substring(0, loc);
		
					ADPerson adPerson = context.retrievePerson(username);
		
					if (adPerson == null) {
		
						//out.println("No Active Directory record found for " + tPerson.username);
		
					    //out.println("\r\n\r\n\r\n");
		
		
		
						continue;
		
					}
		
					//person.replaceAttribute("physicalDeliveryOfficeName", "My Office", context.getContext());
		
		
		
					String adValue = "";
		
					String tValue = "";
		
					
					/* leaving title stuff for later
					
					if (adValue.equals("") && !tValue.equals("")) {
						adPerson.addAttribute("title", tValue, context.getContext(), out);
						
					}
					else if (!tValue.equals("")){
						adPerson.replaceAttribute("title", tValue, context.getContext(), out);
					}
					
					*/
					
					if (username.compareToIgnoreCase("deleeuwm") != 0) {
						//update department
						
						tValue = tPerson.department;
						
						if (tValue.contains("CLERK"))
							tValue = "Clerk";
						else if (tValue.contains("CMH"))
							tValue = "CMH";
						else if (tValue.contains("OCED"))
							tValue = "OCED";
						else if (tValue.contains("PUBLIC HEALTH ADMIN"))
							tValue = "Health Department";
						else if (tValue.contains("PUBLIC HEALTH ENV"))
							tValue = "Environmental Health";
						else if (tValue.contains("PARKS"))
							tValue = "Parks & Recreation";
						else if (tValue.equalsIgnoreCase("District Court Administration") || tValue.equalsIgnoreCase("DISTRICT COURT 14A 1"))
							tValue = "14A 1 District Court";
						else if (tValue.equalsIgnoreCase("DISTRICT COURT 14A 2"))
							tValue = "14A 2 District Court";
						else if (tValue.equalsIgnoreCase("DISTRICT COURT 14A 3"))
							tValue = "14A 3 District Court";
						else if (tValue.equalsIgnoreCase("DISTRICT COURT 14A 4"))
							tValue = "14A 4 District Court";
						else if (tValue.equalsIgnoreCase("HIDTA"))
							tValue = "HIDTA";
						else if (tValue.contains("LAWNET"))
							tValue = "Sheriff LAWNET";
						else if (tValue.equalsIgnoreCase("TRIAL COURT FRIEND OF COURT"))
							tValue = "Trial Court FOC";
						else 
							tValue = toTitleCase(tValue);	
			
						adValue = adPerson.getAttribute("department", out);
						
						if (adValue.equals("") && !tValue.equals("")) {
			
							adPerson.addAttribute("department", tValue, context.getContext(), out);
			
						}
						else if (!tValue.equals("")){
							adPerson.replaceAttribute("department", tValue, context.getContext(), out);
						}
					}
						
					// add department head status to the info field in AD.
					tValue = tPerson.getDepartmentHead();
					if (tValue.equalsIgnoreCase("Y"))
						tValue = "Department Head";
					
					String depthead = adPerson.getAttribute("info",out);
					
					if (depthead.equals("") && tValue.equals("Department Head")) {
						adPerson.addAttribute("info", tValue, context.getContext(), out);
					} else if (depthead.equals("Department Head") && tValue.equals("Department Head")) {
						// do nothing
					} else if (depthead.equals("") && tValue.equals("")) {
						//do nothing
					} else 
						adPerson.removeAttribute("info", tValue, context.getContext(), out);
					
					//add the employee number to the employeeID field in AD.
					tValue = tPerson.getEmpId();
						
					
					String adempID = adPerson.getAttribute("employeeID", out);
					if (adempID.equals("")){
						adPerson.addAttribute("employeeID", tValue, context.getContext(), out);
						
					} else
						adPerson.replaceAttribute("employeeID", tValue, context.getContext(), out);
					
					
					
					
					//Set the supervisor field to the superviosr's distinguished name - if Sheriff department, make Haley Gordon
					// only do this if the username is not deleeuwm - this person is on the Parks Board, but wortks for the Cpnservation District
					if (username.compareToIgnoreCase("deleeuwm") != 0) {
						String supEmail = null;
						supEmail = tPerson.getSupervisorEmail();
						tValue = tPerson.department.toLowerCase();
						// if the supervisor is not filled in in Munis, remove the superviosr from AD
						if (supEmail == null || supEmail.equalsIgnoreCase("")) {
							adValue = adPerson.getAttribute("manager", out);
							if (!adValue.equalsIgnoreCase(""))
								adPerson.removeAttribute("manager",  "", context.getContext(), out);
						}
						else {
							if (tValue.contains("sheriff")) 
								supEmail = "gordonh@washtenaw.org";
								
							
							if (supEmail != null) {
								loc = supEmail.indexOf("@");
								if (loc > -1) {
									String supname = supEmail.substring(0, loc);
									ADPerson adSup = context.retrievePerson(supname);
									if (adSup != null) {
										adValue = adPerson.getAttribute("manager", out);
										if (adValue.equals("")) {
											
											adPerson.addAttribute("manager",adSup.getDistinguishedName(), context.getContext(), out);
							
										}
										else {
											adPerson.removeAttribute("manager", adSup.getDistinguishedName(), context.getContext(), out);
										}
									}
								}
							}
						}
					}
					
					

				}//end for
				stmt.close();
				conn.close();
			}	catch (SQLException e) {
				e.printStackTrace();
			}
		
				
			context.close();
			//System.out.println("ending");
	
		} catch (Exception ex) {
			ex.printStackTrace(out);
			out.println(ex.getMessage());
		}
	}
	
	 private static String toTitleCase(String str) {
	        
	        if(str == null || str.isEmpty())
	            return "";
	        
	        if(str.length() == 1)
	            return str.toUpperCase();
	        
	        //split the string by space
	        String[] parts = str.split(" ");
	        
	        StringBuilder sb = new StringBuilder( str.length() );
	        
	        for(String part : parts){
	 
	            if(part.length() > 1 )                
	                sb.append( part.substring(0, 1).toUpperCase() )
	                .append( part.substring(1).toLowerCase() );                
	            else
	                sb.append(part.toUpperCase());
	            
	            sb.append(" ");
	        }
	        
	        return sb.toString().trim();
	    }
}
