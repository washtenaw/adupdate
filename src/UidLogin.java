/*	
	# -- LDAP server connection info ----
	############  Production WCDC (MS AD / Win2K) ####################
	ldap.classname=org.ewashtenaw.intranet.security.MSActiveDirectory
	ldap.hostname=10.1.1.95
	ldap.port=389
	ldap.rootcontext=DC=ewashtenaw,DC=org
	ldap.authbasedn=
	ldap.emailAttribute=mail
	ldap.queryUsername=student2@ewashtenaw.org
	ldap.queryPassword=4ourwebsite
*/

import java.util.Hashtable;
import javax.naming.*;
import javax.naming.ldap.*;
import javax.naming.directory.*;

public class UidLogin	{
	public static void main (String[] args)	{
	
		Hashtable<String, String> env = new Hashtable<String, String>();
		String adminName = "zopeuser@ewashtenaw.org";
		String adminPassword = "4ourwebsite";
		String ldapURL = "ldap://10.1.1.95:389";
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		//set security credentials, note using simple cleartext authentication
		env.put(Context.SECURITY_AUTHENTICATION,"simple");
		env.put(Context.SECURITY_PRINCIPAL,adminName);
		env.put(Context.SECURITY_CREDENTIALS,adminPassword);
				
		//connect to my domain controller
		env.put(Context.PROVIDER_URL,ldapURL);
		
		try {
 
			//Create the initial directory context
			LdapContext ctx = new InitialLdapContext(env,null);

			//Create the search controls 		
			SearchControls searchCtls = new SearchControls();
		
			//Specify the search scope
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
 
			//specify the LDAP search filter
			String searchFilter = "(&(samAccountName=broganm))";
		
			//Specify the Base for the search
			String searchBase = "DC=ewashtenaw,DC=org";
 
			//initialize counter to total the group members
			int totalResults = 0;
 
			//Specify the attributes to return
			String returnedAtts[]={"*"};
			searchCtls.setReturningAttributes(returnedAtts);
		
			//Search for objects using the filter
			NamingEnumeration<?> answer = ctx.search(searchBase, searchFilter, searchCtls);

			//Loop through the search results
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult)answer.next();
 
				System.out.println(">>>" + sr.getName());
 
				//Print out the groups
 
				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
 
					try {
						for (NamingEnumeration<?> ae = attrs.getAll();ae.hasMore();) {
							Attribute attr = (Attribute)ae.next();
							System.out.println("" + attr.getID() + " ");
/*
							if (attr.getID().equals("extensionAttribute7")) {
								ModificationItem[] mods = new ModificationItem[1];
								mods[0] = new ModificationItem(ctx.ADD_ATTRIBUTE, 
										new BasicAttribute("extensionAttribute7", "lolo"));
								
								
								try {
									ctx.modifyAttributes( sr.getName()+","+searchBase
											//"DC=ewashtenaw,DC=org"
											, mods);
								} catch (javax.naming.directory.AttributeInUseException e) {
									System.out.println("already using that one!");
									e.printStackTrace();
								}

							}
*/
							for (NamingEnumeration<?> e = attr.getAll();e.hasMore();totalResults++) {
								//e.next();
								System.out.println("\t" +  e.next());
								//System.out.println("\t" +  totalResults + ". " +  e.next());
							}
 
						}
 
					}	 
					catch (NamingException e)	{
						System.err.println("Error:\n");
						e.printStackTrace();
					}
				
				}
				
			}
 
			System.out.println("Total groups: " + totalResults);
			ctx.close();
 
		} 
		
		catch (NamingException e) {
			System.err.println("Problem searching directory: " + e);
           	}
	}
}
