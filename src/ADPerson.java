import java.io.PrintStream;import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.ModificationItem;
import javax.naming.ldap.LdapContext;
public class ADPerson {
	private final String distinguishedName; //DO NOT CHANGE - used when modifying attributes
	
	Attributes attrs;
	
	
	public ADPerson(String dName, Attributes attrs) {
		distinguishedName = new String(dName);
		this.attrs = attrs;
	}
		public String getDistinguishedName() {		return distinguishedName;	}
	
	public String getAttribute(String attributeName, PrintStream out) {
		StringBuffer value = new StringBuffer("");
		try {
			Attribute attr = (Attribute)attrs.get(attributeName);
			NamingEnumeration<?> e = attr.getAll();
			while (e.hasMore()) {
				value.append(e.next() + " ");
				//System.out.println("\t" +  e.next());
			}
		} catch (NamingException e) {
			e.printStackTrace(out);
		} catch (NullPointerException e) {
			//e.printStackTrace();
			//out.println("No assigned value for field " + attributeName);
			return "";
			
		}
		return value.toString().trim();
	}
	
	public void addAttribute(String attributeName, String attributeValue, LdapContext ctx, PrintStream out) {
		ModificationItem[] mods = new ModificationItem[1];
		mods[0] = new ModificationItem(LdapContext.ADD_ATTRIBUTE, 
				new BasicAttribute(attributeName, attributeValue));
		try {
			out.println("Adding value " + attributeValue + " to field " + attributeName);
			ctx.modifyAttributes( distinguishedName, mods);
		} catch (javax.naming.directory.AttributeInUseException e) {			replaceAttribute(attributeName, attributeValue, ctx, out);
			//out.println("Attempted add to single value attribute!");
			//e.printStackTrace(out);
		} catch (NamingException e) {
			e.printStackTrace(out);
		}
	}

	public void replaceAttribute(String attributeName, String attributeValue, LdapContext ctx, PrintStream out) {
		ModificationItem[] mods = new ModificationItem[1];
		mods[0] = new ModificationItem(LdapContext.REPLACE_ATTRIBUTE, 
				new BasicAttribute(attributeName, attributeValue));
		try {
			//out.println("Replacing value in field " + attributeName + " with " + attributeValue);
			ctx.modifyAttributes( distinguishedName, mods);
		} catch (NamingException e) {
			e.printStackTrace(out);
		}
	}

	public void removeAttribute(String attributeName, String attributeValue, LdapContext ctx, PrintStream out) {
		ModificationItem[] mods = new ModificationItem[1];
		mods[0] = new ModificationItem(LdapContext.REMOVE_ATTRIBUTE, 
				new BasicAttribute(attributeName));
		try {
			//out.println("Removing value in field " + attributeName);
			ctx.modifyAttributes( distinguishedName, mods);
		} catch (NamingException e) {
			e.printStackTrace(out);
		}
	}
}


