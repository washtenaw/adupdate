import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;


public class ADContext {

	LdapContext ctx;
	
	public ADContext() {
		this.initializeContext();
	}

	public void initializeContext() {

		Hashtable<String, String> env = new Hashtable<String, String>();
		String adminName = "zopeuser@ewashtenaw.org";
		String adminPassword = "4ourwebsite";
		String ldapURL = "ldap://10.1.1.95:389";
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		//set security credentials, note using simple cleartext authentication
		env.put(Context.SECURITY_AUTHENTICATION,"simple");
		env.put(Context.SECURITY_PRINCIPAL,adminName);
		env.put(Context.SECURITY_CREDENTIALS,adminPassword);

		//connect to my domain controller
		env.put(Context.PROVIDER_URL,ldapURL);

		try {
			//Create the initial directory context
			ctx = new InitialLdapContext(env,null);
		} 

		catch (NamingException e) {
			System.err.println("Problem searching directory: " + e);
		}
	}
	
	public LdapContext getContext() {
		return ctx;
	}
	
	public void close() {
		try {
			ctx.close();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ADPerson retrievePerson(String searchName) {

		ADPerson person = null;

		//Create the search controls 		
		SearchControls searchCtls = new SearchControls();

		//Specify the search scope
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		//specify the LDAP search filter
		String searchFilter = "(&(samAccountName=" + searchName +"))";

		//Specify the Base for the search
		String searchBase = "DC=ewashtenaw,DC=org";

		//initialize counter to total the group members
		//int totalResults = 0;

		//Specify the attributes to return
		String returnedAtts[]={"*"};
		searchCtls.setReturningAttributes(returnedAtts);
		try {
			//Search for objects using the filter
			NamingEnumeration<?> answer = ctx.search(searchBase, searchFilter, searchCtls);
			//Loop through the search results
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult)answer.next();
				
				//System.out.println("Retrieved account: " + sr.getName());
						
				//Print out the groups

				Attributes attrs = sr.getAttributes();
				person = new ADPerson(sr.getName()+","+searchBase, attrs );
		//--- uncomment from here
				/*		
				if (attrs != null) {
					try {
						for (NamingEnumeration ae = attrs.getAll();ae.hasMore();) {
							Attribute attr = (Attribute)ae.next();
							System.out.println(">" + attr.getID() + " ");
							for (NamingEnumeration e = attr.getAll();e.hasMore();totalResults++) {
								System.out.println("\t" +  e.next());
							}
						}
					}	 
					catch (NamingException e)	{
						System.err.println("Error:\n");
						e.printStackTrace();
					}
				}
				*/
		//--- to here to show attributes and values
			}
			//System.out.println("Total groups: " + totalResults);
			//ctx.close();
		}catch (NamingException e) {
			System.err.println("Problem searching directory: " + e);
			return null;
		}
		return person;
	}
}

/*
if (attr.getID().equals("extensionAttribute7")) {
	ModificationItem[] mods = new ModificationItem[1];
	mods[0] = new ModificationItem(ctx.ADD_ATTRIBUTE, 
			new BasicAttribute("extensionAttribute7", "lolo"));


	try {
		ctx.modifyAttributes( sr.getName()+","+searchBase
				//"DC=ewashtenaw,DC=org"
				, mods);
	} catch (javax.naming.directory.AttributeInUseException e) {
		System.out.println("already using that one!");
		e.printStackTrace();
	}

}
	 */
